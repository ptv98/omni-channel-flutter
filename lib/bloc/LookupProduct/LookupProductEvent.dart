abstract class LookupProductEvent {
  const LookupProductEvent();
}

class LookupProductRequest extends LookupProductEvent {
  const LookupProductRequest(this.text);
  final String text;
}
