import 'package:equatable/equatable.dart';
import 'package:omnichannel_flutter/constant/Status.dart';
import 'package:omnichannel_flutter/data/modals/LookupProduct.dart';

class LookupProductState extends Equatable {
  const LookupProductState({this.status, this.data});

  final List<LookupProductModal> data;
  final Status status;

  LookupProductState copyWith({List<LookupProductModal> data, Status status}) =>
      LookupProductState(
          data: data ?? this.data, status: status ?? this.status);

  @override
  List<Object> get props => [data, status];
}
