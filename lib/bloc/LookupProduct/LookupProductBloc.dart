import 'dart:developer';

import 'package:bloc/bloc.dart';
import 'package:omnichannel_flutter/bloc/LookupProduct/LookupProductEvent.dart';
import 'package:omnichannel_flutter/bloc/LookupProduct/LookupProductState.dart';
import 'package:omnichannel_flutter/constant/Status.dart';
import 'package:omnichannel_flutter/data/modals/LookupProduct.dart';
import 'package:omnichannel_flutter/data/repository/remote_repository.dart';

class LookupProductBloc extends Bloc<LookupProductEvent, LookupProductState> {
  LookupProductBloc() : super(LookupProductState(status: Status.initial, data: []));

  @override
  Stream<LookupProductState> mapEventToState(LookupProductEvent event) async* {
    if (event is LookupProductRequest) {
      yield state.copyWith(status: Status.loading, data: []);
      try {
        final result = await RemoteRepository.lookupProduct(event.text);
        List<LookupProductModal> data = [];
        result.forEach((element) {
          element.variants.forEach((variant) {
            data.add(LookupProductModal(
              name: element.name,
              price: variant.price,
              attributes: variant.attributes,
              variantId: variant.id,
              weight: element.weight,
              productIdRef: element.id,
            ));
          });
        });
        yield state.copyWith(status: Status.success, data: data);
      } catch (e) {
        yield state.copyWith(status: Status.fail, data: []);
      }
    }
  }
}
