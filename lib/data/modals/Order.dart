import 'package:equatable/equatable.dart';
import 'package:omnichannel_flutter/data/modals/Export.dart';
import 'package:omnichannel_flutter/data/modals/Location.dart';
import 'package:omnichannel_flutter/data/modals/ManagementProductResponse.dart';
import 'package:omnichannel_flutter/data/modals/Stock.dart';

class OrdersPaging extends Equatable {
  OrdersPaging({
    this.count,
    this.items,
    this.pageInfo,
  });

  final int count;
  final List<Order> items;
  final PageInfo pageInfo;

  OrdersPaging copyWith({
    int count,
    List<Order> items,
    PageInfo pageInfo,
  }) =>
      OrdersPaging(
        count: count ?? this.count,
        items: items ?? this.items,
        pageInfo: pageInfo ?? this.pageInfo,
      );

  factory OrdersPaging.fromJson(Map<String, dynamic> json) => OrdersPaging(
        count: json["count"],
        items: List<Order>.from(json["items"].map((x) => Order.fromJson(x))),
        pageInfo: PageInfo.fromJson(json["pageInfo"]),
      );

  Map<String, dynamic> toJson() => {
        "count": count,
        "items": List<dynamic>.from(items.map((x) => x.toJson())),
        "pageInfo": pageInfo.toJson(),
      };

  @override
  List<Object> get props => [count, items, pageInfo];
}

class Order extends Equatable {
  Order({
    this.id,
    this.itemId,
    this.minetype,
    this.customerName,
    this.phoneNumber,
    this.amount,
    this.cod,
    this.totalWeight,
    this.status,
    this.stockId,
    this.shippingPartnerId,
    this.dateCreated,
    this.conversationId,
    this.chanelId,
    this.cartItems,
    this.address,
    this.cityCode,
    this.districtCode,
    this.wardCode,
    this.ward,
    this.city,
    this.district,
    this.shippingCode,
    this.shippingFee,
    this.internalNote,
    this.externalNote,
    this.bankPayment,
    this.cashPayment,
    this.cardPayment,
    this.otherPayment,
    this.recipientName,
    this.recipientPhoneNumber,
    this.discount,
  });

  final String id;
  final int itemId;
  final int minetype;
  final String customerName;
  final String phoneNumber;
  final int amount;
  final int cod;
  final int totalWeight;
  final int status;
  final String stockId;
  final String shippingPartnerId;
  final int dateCreated;
  final String conversationId;
  final dynamic chanelId;
  final List<CartItem> cartItems;
  final String address;
  final int cityCode;
  final int districtCode;
  final int wardCode;
  final Ward ward;
  final City city;
  final District district;
  final String shippingCode;
  final ShippingFee shippingFee;
  final String internalNote;
  final String externalNote;
  final int bankPayment;
  final int cashPayment;
  final int cardPayment;
  final int otherPayment;
  final int discount;
  final String recipientName;
  final String recipientPhoneNumber;

  static Order get empty {
    return Order(
        minetype: 1,
        cartItems: [],
        discount: 0,
        cardPayment: 0,
        otherPayment: 0,
        cashPayment: 0,
        bankPayment: 0);
  }

  Order copyWith({
    String id,
    int itemId,
    int minetype,
    String customerName,
    String phoneNumber,
    int amount,
    int cod,
    int totalWeight,
    int status,
    String stockId,
    String shippingPartnerId,
    int dateCreated,
    String conversationId,
    dynamic chanelId,
    List<CartItem> cartItems,
    String address,
    int cityCode,
    bool isClearCity = false,
    int districtCode,
    bool isClearDistrict = false,
    int wardCode,
    bool isClearWard = false,
    Ward ward,
    City city,
    District district,
    String shippingCode,
    ShippingFee shippingFee,
    String internalNote,
    String externalNote,
    int bankPayment,
    int cashPayment,
    int cardPayment,
    int otherPayment,
    int discount,
    String recipientName,
    String recipientPhoneNumber,
  }) =>
      Order(
        id: id ?? this.id,
        itemId: itemId ?? this.itemId,
        minetype: minetype ?? this.minetype,
        customerName: customerName ?? this.customerName,
        phoneNumber: phoneNumber ?? this.phoneNumber,
        amount: amount ?? this.amount,
        cod: cod ?? this.cod,
        totalWeight: totalWeight ?? this.totalWeight,
        status: status ?? this.status,
        stockId: stockId ?? this.stockId,
        shippingPartnerId: shippingPartnerId ?? this.shippingPartnerId,
        dateCreated: dateCreated ?? this.dateCreated,
        conversationId: conversationId ?? this.conversationId,
        chanelId: chanelId ?? this.chanelId,
        cartItems: cartItems ?? this.cartItems,
        address: address ?? this.address,
        cityCode: isClearCity ? null : (cityCode ?? this.cityCode),
        districtCode:
            isClearDistrict ? null : (districtCode ?? this.districtCode),
        wardCode: isClearWard ? null : (wardCode ?? this.wardCode),
        ward: ward ?? this.ward,
        city: city ?? this.city,
        district: district ?? this.district,
        shippingCode: shippingCode ?? this.shippingCode,
        shippingFee: shippingFee ?? this.shippingFee,
        internalNote: internalNote ?? this.internalNote,
        externalNote: externalNote ?? this.externalNote,
        bankPayment: bankPayment ?? this.bankPayment,
        cashPayment: cashPayment ?? this.cashPayment,
        cardPayment: cardPayment ?? this.cardPayment,
        otherPayment: otherPayment ?? this.otherPayment,
        discount: discount ?? this.discount,
        recipientName: recipientName ?? this.recipientName,
        recipientPhoneNumber: recipientPhoneNumber ?? this.recipientPhoneNumber,
      );

  factory Order.fromJson(Map<String, dynamic> json) => Order(
      id: json["_id"],
      itemId: json["id"],
      minetype: json["minetype"],
      customerName: json["customer_name"],
      phoneNumber: json["phone_number"],
      amount: json["amount"],
      cod: json["COD"],
      totalWeight: json["total_weight"],
      status: json["status"],
      stockId: json["stock_id"],
      shippingPartnerId: json["shipping_partner_id"] == null
          ? null
          : json["shipping_partner_id"],
      dateCreated: json["date_created"],
      conversationId:
          json["conversation_id"] == null ? null : json["conversation_id"],
      chanelId: json["chanel_id"],
      cartItems: List<CartItem>.from(
          json["cart_items"].map((x) => CartItem.fromJson(x))),
      address: json["address"] == null ? null : json["address"],
      cityCode: json["city_code"] == null ? null : json["city_code"],
      districtCode:
          json["district_code"] == null ? null : json["district_code"],
      wardCode: json["ward_code"] == null ? null : json["ward_code"],
      ward: json["ward"] == null ? null : Ward.fromJson(json["ward"]),
      city: json["city"] == null ? null : City.fromJson(json["city"]),
      district:
          json["district"] == null ? null : District.fromJson(json["district"]),
      shippingCode:
          json["shipping_code"] == null ? null : json["shipping_code"],
      shippingFee: json["shipping_fee"] == null
          ? null
          : ShippingFee.fromJson(json["shipping_fee"]),
      internalNote: json["internal_note"],
      externalNote: json["external_note"],
      bankPayment: json["bank_payment"],
      cashPayment: json["cash_payment"],
      cardPayment: json["card_payment"],
      otherPayment: json["other_payment"],
      recipientName: json["recipient_name"],
      recipientPhoneNumber: json["recipient_phone_number"],
      discount: json["discount"]);

  Map<String, dynamic> toJson() {
    final data = {
      "minetype": minetype,
      "customer_name": customerName,
      "phone_number": phoneNumber,
      "stock_id": stockId,
      "cart_items": List<dynamic>.from(cartItems.map((x) => x.toJson())),
      "address": address == null ? null : address,
      "city_code": cityCode == null ? null : cityCode,
      "district_code": districtCode == null ? null : districtCode,
      "ward_code": wardCode == null ? null : wardCode,
      "internal_note": internalNote,
      "external_note": externalNote,
      "bank_payment": bankPayment,
      "card_payment": cardPayment,
      "other_payment": otherPayment,
      "recipient_name": recipientName,
      "recipient_phone_number": recipientPhoneNumber,
      "discount": discount,
    };
    data.removeWhere((key, value) => key == null || value == null);
    return data;
  }

  @override
  List<Object> get props => [
        id,
        itemId,
        minetype,
        customerName,
        phoneNumber,
        amount,
        cod,
        totalWeight,
        status,
        stockId,
        shippingPartnerId,
        dateCreated,
        conversationId,
        chanelId,
        cartItems,
        address,
        cityCode,
        districtCode,
        wardCode,
        ward,
        city,
        district,
        shippingCode,
        shippingFee,
        internalNote,
        externalNote,
        bankPayment,
        cashPayment,
        cardPayment,
        otherPayment,
        recipientName,
        recipientPhoneNumber,
        discount,
      ];
}

class ShippingFee extends Equatable {
  ShippingFee({
    this.fee,
  });

  final int fee;

  ShippingFee copyWith({
    int fee,
  }) =>
      ShippingFee(
        fee: fee ?? this.fee,
      );

  factory ShippingFee.fromJson(Map<String, dynamic> json) => ShippingFee(
        fee: json["fee"],
      );

  Map<String, dynamic> toJson() => {
        "fee": fee,
      };

  @override
  List<Object> get props => [fee];
}

class CartItem extends Equatable {
  CartItem({
    this.productIdRef,
    this.variantId,
    this.qty,
    this.price,
    this.weight,
    this.productName,
    this.attributes,
  });

  String productIdRef;
  int variantId;
  int qty;
  int price;
  int weight;
  String productName;
  List<Attributes> attributes;

  CartItem copyWith({
    String productIdRef,
    int variantId,
    int qty,
    int price,
    int weight,
    String productName,
    List<Attributes> attributes,
  }) =>
      CartItem(
          productIdRef: productIdRef ?? this.productIdRef,
          variantId: variantId ?? this.variantId,
          qty: qty ?? this.qty,
          price: price ?? this.price,
          attributes: attributes ?? this.attributes,
          productName: productName ?? this.productName,
          weight: weight ?? this.weight);

  factory CartItem.fromJson(Map<String, dynamic> json) => CartItem(
        productIdRef: json["product_id_ref"],
        variantId: json["variant_id"],
        qty: json["qty"],
        price: json["price"],
        weight: json["weight"],
        productName: json["product_name"],
        attributes: List<Attributes>.from(
            json["attributes"].map((x) => Attributes.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "product_id_ref": productIdRef,
        "variant_id": variantId,
        "qty": qty,
        // "price": price,
        // "weight": weight,
        // "product_name": productName,
        // "attributes": List<dynamic>.from(attributes.map((x) => x.toJson())),
      };

  @override
  List<Object> get props =>
      [productIdRef, variantId, qty, price, weight, productName, attributes];
}

class CreatedByUser {
  CreatedByUser({
    this.displayName,
  });

  String displayName;

  factory CreatedByUser.fromJson(Map<String, dynamic> json) => CreatedByUser(
        displayName: json["display_name"],
      );

  Map<String, dynamic> toJson() => {
        "display_name": displayName,
      };
}
