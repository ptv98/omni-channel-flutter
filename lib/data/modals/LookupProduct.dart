import 'package:equatable/equatable.dart';
import 'package:omnichannel_flutter/data/modals/ManagementProductResponse.dart';

class LookupProductModal extends Equatable {
  const LookupProductModal(
      {this.name,
      this.attributes,
      this.price,
      this.variantId,
      this.weight,
      this.productIdRef});

  final String name;
  final List<Attributes> attributes;
  final int variantId;
  final int price;
  final int weight;
  final String productIdRef;

  @override
  List<Object> get props => [name, attributes, price, variantId, weight, productIdRef];
}
