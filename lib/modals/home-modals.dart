import 'package:flutter/cupertino.dart';

class ScreenTheme {
  final Color color;
  final String title;
  final IconData icon;
  ScreenTheme({this.color, this.title, this.icon});
}