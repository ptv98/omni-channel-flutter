import 'package:equatable/equatable.dart';
import 'package:omnichannel_flutter/constant/Status.dart';
import 'package:omnichannel_flutter/data/modals/Order.dart';

class OrderState extends Equatable {
  const OrderState({this.ordersPaging, this.confirmOrderStatus});

  final StateOrdersPaging ordersPaging;
  final Status confirmOrderStatus;
  // final StateCreateOrder createOrder;

  OrderState copyWith(
          {StateOrdersPaging ordersPaging, Status confirmOrderStatus}) =>
      OrderState(
          ordersPaging: ordersPaging ?? this.ordersPaging,
          confirmOrderStatus: confirmOrderStatus ?? this.confirmOrderStatus);

  @override
  List<Object> get props => [ordersPaging, confirmOrderStatus];
}

class StateOrdersPaging extends Equatable {
  const StateOrdersPaging({this.status, this.data});

  final Status status;
  final OrdersPaging data;

  @override
  List<Object> get props => [status, data];

  StateOrdersPaging copyWith({Status status, OrdersPaging data}) =>
      StateOrdersPaging(status: status ?? this.status, data: data ?? this.data);
}

// class StateCreateOrder extends Equatable {
//   const StateCreateOrder({this.status, this.payload});
//   final Status status;
//   final
// }
