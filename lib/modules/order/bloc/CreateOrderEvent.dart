import 'package:omnichannel_flutter/data/modals/Location.dart';
import 'package:omnichannel_flutter/data/modals/Order.dart';

abstract class CreateOrderEvent {
  const CreateOrderEvent();
}

class UpdatePayloadMineType extends CreateOrderEvent {
  const UpdatePayloadMineType(this.minetype);
  final int minetype;
}

class UpdatePayloadPhoneNumber extends CreateOrderEvent {
  const UpdatePayloadPhoneNumber(this.phoneNumber);
  final String phoneNumber;
}

class UpdatePayloadCustomerName extends CreateOrderEvent {
  const UpdatePayloadCustomerName(this.customerName);
  final String customerName;
}

class UpdatePayloadRecipientPhoneNumber extends CreateOrderEvent {
  const UpdatePayloadRecipientPhoneNumber(this.phoneNumber);
  final String phoneNumber;
}

class UpdatePayloadRecipientName extends CreateOrderEvent {
  const UpdatePayloadRecipientName(this.text);
  final String text;
}

class UpdatePayloadCity extends CreateOrderEvent {
  const UpdatePayloadCity(this.code);
  final int code;
}

class UpdatePayloadDistrict extends CreateOrderEvent {
  const UpdatePayloadDistrict(this.code);
  final int code;
}

class UpdatePayloadWard extends CreateOrderEvent {
  const UpdatePayloadWard(this.code);
  final int code;
}

class UpdatePayloadAddress extends CreateOrderEvent {
  const UpdatePayloadAddress(this.text);
  final String text;
}

class UpdatePayloadAddProduct extends CreateOrderEvent {
  const UpdatePayloadAddProduct(this.cartItem);
  final CartItem cartItem;
}

class UpdatePayloadRemoveProduct extends CreateOrderEvent {
  const UpdatePayloadRemoveProduct(this.index);
  final int index;
}

class UpdatePayloadAddQuantity extends CreateOrderEvent {
  const UpdatePayloadAddQuantity(this.index);
  final int index;
}

class UpdatePayloadRemoveQuantity extends CreateOrderEvent {
  const UpdatePayloadRemoveQuantity(this.index);
  final int index;
}

class UpdatePayloadStockId extends CreateOrderEvent {
  const UpdatePayloadStockId(this.id);
  final String id;
}

class UpdatePayloadDiscount extends CreateOrderEvent {
  const UpdatePayloadDiscount(this.value);
  final int value;
}

class UpdatePayloadBankPayment extends CreateOrderEvent {
  const UpdatePayloadBankPayment(this.value);
  final int value;
}

class UpdatePayloadCardPayment extends CreateOrderEvent {
  const UpdatePayloadCardPayment(this.value);
  final int value;
}

class UpdatePayloadCashPayment extends CreateOrderEvent {
  const UpdatePayloadCashPayment(this.value);
  final int value;
}

class UpdatePayloadOtherPayment extends CreateOrderEvent {
  const UpdatePayloadOtherPayment(this.value);
  final int value;
}

class UpdatePayloadInternalNote extends CreateOrderEvent {
  const UpdatePayloadInternalNote(this.value);
  final String value;
}

class UpdatePayloadCustomerNote extends CreateOrderEvent {
  const UpdatePayloadCustomerNote(this.value);
  final String value;
}

class CreateOrderRequest extends CreateOrderEvent {
  const CreateOrderRequest({this.callback});
  final Function(bool) callback;
}