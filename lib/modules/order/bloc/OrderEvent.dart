import 'package:omnichannel_flutter/data/modals/Location.dart';
import 'package:omnichannel_flutter/data/modals/Order.dart';

enum OrdersPagingType { refresh, load_more }

abstract class OrderEvent {
  const OrderEvent();
}

class OrdersPagingEvent extends OrderEvent {
  const OrdersPagingEvent({this.type});
  final OrdersPagingType type;
}

class ConfirmOrderEvent extends OrderEvent {
  const ConfirmOrderEvent(this.id, this.callback);
  final String id;
  final Function(bool) callback;
}