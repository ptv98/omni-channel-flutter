import 'dart:developer';

import 'package:bloc/bloc.dart';
import 'package:omnichannel_flutter/bloc/LocationData/LocationDataBloc.dart';
import 'package:omnichannel_flutter/bloc/LocationData/LocationDataEvent.dart';
import 'package:omnichannel_flutter/constant/Status.dart';
import 'package:omnichannel_flutter/data/modals/Order.dart';
import 'package:omnichannel_flutter/data/repository/remote_repository.dart';
import 'package:omnichannel_flutter/modules/order/bloc/CreateOrderEvent.dart';
import 'package:omnichannel_flutter/modules/order/bloc/CreateOrderState.dart';

class CreateOrderBloc extends Bloc<CreateOrderEvent, CreateOrderState> {
  CreateOrderBloc(this.locationDataBloc)
      : super(CreateOrderState(
            status: Status.initial,
            payload: Order.empty));
  final LocationDataBloc locationDataBloc;

  @override
  Stream<CreateOrderState> mapEventToState(CreateOrderEvent event) async* {
    if (event is UpdatePayloadMineType)
      yield state.copyWith(
          payload: state.payload.copyWith(minetype: event.minetype));
    if (event is UpdatePayloadPhoneNumber)
      yield state.copyWith(
          payload: state.payload.copyWith(phoneNumber: event.phoneNumber));
    if (event is UpdatePayloadCustomerName)
      yield state.copyWith(
          payload: state.payload.copyWith(customerName: event.customerName));
    if (event is UpdatePayloadRecipientPhoneNumber)
      yield state.copyWith(
          payload:
              state.payload.copyWith(recipientPhoneNumber: event.phoneNumber));
    if (event is UpdatePayloadRecipientName)
      yield state.copyWith(
          payload: state.payload.copyWith(recipientName: event.text));
    if (event is UpdatePayloadCity) {
      if (state.payload.cityCode != null &&
          state.payload.cityCode != event.code) {
        yield state.copyWith(
            payload: state.payload.copyWith(
                cityCode: event.code,
                isClearDistrict: true,
                isClearWard: true));
      } else {
        yield state.copyWith(
            payload: state.payload.copyWith(cityCode: event.code));
      }
      locationDataBloc.add(LocationDataEventGetDistricts(cityCode: event.code));
    }
    if (event is UpdatePayloadDistrict) {
      if (state.payload.districtCode != null &&
          state.payload.districtCode != event.code) {
        yield state.copyWith(
            payload: state.payload
                .copyWith(districtCode: event.code, isClearWard: true));
      } else {
        yield state.copyWith(
            payload: state.payload.copyWith(districtCode: event.code));
      }
      locationDataBloc.add(LocationDataEventGetWards(districtCode: event.code));
    }
    if (event is UpdatePayloadWard) {
      yield state.copyWith(
          payload: state.payload.copyWith(wardCode: event.code));
    }
    if (event is UpdatePayloadAddress) {
      yield state.copyWith(
          payload: state.payload.copyWith(address: event.text));
    }
    if (event is UpdatePayloadAddProduct) {
      final index = state.payload.cartItems.indexWhere((element) =>
          element.productIdRef == event.cartItem.productIdRef &&
          element.variantId == event.cartItem.variantId);
      if (index == -1) {
        List<CartItem> items = List.from(state.payload.cartItems);
        items.add(event.cartItem);
        yield state.copyWith(payload: state.payload.copyWith(cartItems: items));
      } else {
        this.add(UpdatePayloadAddQuantity(index));
      }
    }
    if (event is UpdatePayloadAddQuantity) {
      List<CartItem> items = List.from(state.payload.cartItems);
      final item = state.payload.cartItems[event.index];
      int qty = item.qty + 1;
      final newItem = item.copyWith(qty: qty);
      items.removeAt(event.index);
      items.insert(event.index, newItem);
      yield state.copyWith(payload: state.payload.copyWith(cartItems: items));
    }
    if (event is UpdatePayloadRemoveQuantity) {
      List<CartItem> items = List.from(state.payload.cartItems);
      final item = state.payload.cartItems[event.index];
      int qty = item.qty - 1;
      if (qty == 0) {
        items.removeAt(event.index);
      } else {
        final newItem = item.copyWith(qty: qty);
        items.removeAt(event.index);
        items.insert(event.index, newItem);
      }
      yield state.copyWith(payload: state.payload.copyWith(cartItems: items));
    }
    if (event is UpdatePayloadStockId) {
      yield state.copyWith(payload: state.payload.copyWith(stockId: event.id));
    }
    if (event is UpdatePayloadDiscount) {
      yield state.copyWith(payload: state.payload.copyWith(discount: event.value));
    }
    if (event is UpdatePayloadBankPayment) {
      yield state.copyWith(payload: state.payload.copyWith(bankPayment: event.value));
    }
    if (event is UpdatePayloadCardPayment) {
      yield state.copyWith(payload: state.payload.copyWith(cardPayment: event.value));
    }
    if (event is UpdatePayloadOtherPayment) {
      yield state.copyWith(payload: state.payload.copyWith(otherPayment: event.value));
    }
    if (event is UpdatePayloadInternalNote) {
      yield state.copyWith(payload: state.payload.copyWith(internalNote: event.value));
    }
    if (event is UpdatePayloadCustomerNote) {
      yield state.copyWith(payload: state.payload.copyWith(externalNote: event.value));
    }
    if (event is CreateOrderRequest) {
      yield state.copyWith(status: Status.loading);
      try {
        final result = await RemoteRepository.createOrder(state.payload);
        event.callback?.call(true);
        yield state.copyWith(status: Status.success, payload: result);
      } catch (e) {
        event.callback?.call(false);
        yield state.copyWith(status: Status.fail);
      }
    }
  }
}
