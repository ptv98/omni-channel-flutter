import 'package:equatable/equatable.dart';
import 'package:omnichannel_flutter/constant/Status.dart';
import 'package:omnichannel_flutter/data/modals/Order.dart';

class CreateOrderState extends Equatable {
  const CreateOrderState({this.status, this.payload});

  final Status status;
  final Order payload;

  CreateOrderState copyWith({Status status, Order payload}) =>
      CreateOrderState(
          status: status ?? this.status, payload: payload ?? this.payload);

  @override
  List<Object> get props => [status, payload];
}