import 'dart:developer';

import 'package:after_layout/after_layout.dart';
import "package:collection/collection.dart";
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_sticky_header/flutter_sticky_header.dart';
import 'package:intl/intl.dart';
import 'package:omnichannel_flutter/assets/png-jpg/PngJpg.dart';
import 'package:omnichannel_flutter/common/colors/Colors.dart';
import 'package:omnichannel_flutter/data/modals/Order.dart';
import 'package:omnichannel_flutter/model/orderData.dart';
import 'package:omnichannel_flutter/modules/home/screens/listOrderScreen/widget/orderItem.dart';
import 'package:omnichannel_flutter/modules/order/bloc/OrderBloc.dart';
import 'package:omnichannel_flutter/modules/order/bloc/OrderEvent.dart';
import 'package:omnichannel_flutter/modules/order/bloc/OrderState.dart';
import 'package:omnichannel_flutter/utis/date.dart';
import 'package:omnichannel_flutter/utis/ui/main.dart';
import 'package:omnichannel_flutter/widgets/Button/main.dart';
import 'package:omnichannel_flutter/widgets/CustomAppBar/main.dart';

final formatCurrency = new NumberFormat("#,##0.00", "en_US");

class ListOrderScreen extends StatefulWidget {
  @override
  ListOrderScreenState createState() => ListOrderScreenState();
}

class ListOrderScreenState extends State<ListOrderScreen>
    with AfterLayoutMixin {
  List<OrderData> _listOderData = [];
  List<String> _listTitle = [];
  Map<dynamic, List<OrderData>> _dataOrderToShow;
  ScrollController controllerOrderData;

  void initState() {
    super.initState();
  }

  void afterFirstLayout(BuildContext context) {
    // BlocProvider.of<OrderBloc>(context).add(OrdersPagingEvent(type: OrdersPagingType.refresh));
  }

  _buildSectionList(List<Order> data) {
    final a = groupBy(
        data,
        (Order e) => convertMilisecToDateTimeReadable(
            e.dateCreated, DateFormat('dd/MM/yyyy')));
    final List<Widget> b = [];
    a.forEach((key, value) => b.add(buildSection(key, value)));
    return b;
  }

  onRefreshData() async {
    BlocProvider.of<OrderBloc>(context).add(OrdersPagingEvent(type: OrdersPagingType.refresh));
  }

  _showBottomSheet(BuildContext context, String id) {
    final orderBloc = BlocProvider.of<OrderBloc>(context);

    showModalBottomSheet(
      context: context,
      backgroundColor: Colors.transparent,
      builder: (context) => Container(
        constraints:
            BoxConstraints(minHeight: MediaQuery.of(context).size.height * 0.3),
        decoration: BoxDecoration(
          color: AppColors.bone,
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(16), topLeft: Radius.circular(16)),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            AppButton(
              title: 'Xác nhận đơn',
              onPressed: () {
                orderBloc.add(ConfirmOrderEvent(
                    id,
                    (isSuccess) => showSnackBar(context,
                        text: isSuccess
                            ? 'Xác nhận thành công'
                            : 'Xác nhận thất bại')));
                Navigator.pop(context);
              },
            ),
            SizedBox(
              height: 8,
            ),
            AppButton(
              title: 'Sửa đơn',
              color: AppColors.languidLavender,
            ),
            SizedBox(
              height: 8,
            ),
            AppButton(
              title: 'Tạo bản sao',
              color: AppColors.silverMetallic,
            ),
          ],
        ),
      ),
    );
  }

  Widget buildSection(String title, List<Order> listOrderData) {
    return SliverStickyHeader(
      header: Container(
        padding: const EdgeInsets.all(16),
        color: Color(0xffEFF4F7),
        child: Row(
          children: [
            Text(
              "Ngày " + title,
              style: TextStyle(
                fontSize: 12,
                //fontFamily: I,
                color: Colors.black,
              ),
            ),
            SizedBox(
              width: 10,
            ),
          ],
        ),
      ),
      sliver: SliverList(
        delegate: SliverChildBuilderDelegate(
          (context, index) {
            var item = listOrderData[index];
            return OrderItem(
              key: Key(item.id),
              onPress: () async {},
              onPressOption: () => _showBottomSheet(context, item.id),
              amount: formatCurrency.format(item.amount),
              description: convertCardItemsToDescription(item.cartItems),
              status: item.status,
              sellOn: item.conversationId != null ? 'FB' : null,
              idExport: item.itemId.toString(),
              dateCreated: convertMilisecToDateTimeReadable(
                  item.dateCreated, DateFormat('dd/MM/yyyy')),
              name: item.customerName,
              phoneNumber: item.phoneNumber,
            );
          },
          childCount: listOrderData.length,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<OrderBloc>(
      create: (context) => OrderBloc(),
      child: Scaffold(
        appBar: CustomAppBar(
          title: 'Danh sách đơn hàng',
        ),
        // floatingActionButton: FloatingActionButton(
        //   backgroundColor: AppColors.sage,
        //   child: Icon(Icons.add),
        //   onPressed: () async {
        //     final result = await Navigator.pushNamed(context, "/createOrder");
        //     onRefreshData();
        //   },
        // ),
        body: WillPopScope(
          onWillPop: () {
            Navigator.pop(context, true);
          },
          child: BlocBuilder<OrderBloc, OrderState>(
            builder: (context, state) {
              log('message' + state.ordersPaging.toString());
              
              return state.ordersPaging.data.items.length == 0
                  ? Center(
                child: Padding(
                  padding: EdgeInsets.only(left: 30, right: 30, top: 90),
                  child: Column(
                    children: [
                      Image.asset(
                        PngJpg.imgNullData,
                        width: 150,
                        height: 150,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        "Chưa có đơn hàng",
                        style: TextStyle(fontSize: 14, color: Colors.grey),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                    ],
                  ),
                ),
              )
                  : Container(
                //margin: EdgeInsets.only(top: 15),
                color: Color(0xffEFF4F7),
                child: CustomScrollView(
                  controller: controllerOrderData,
                  slivers: _buildSectionList(state.ordersPaging.data.items),
                ),
              );
            }
          ),
        ),
      ),
    );
  }
}

extension Helper on ListOrderScreenState {
  String convertCardItemsToDescription(List<CartItem> cartItems) {
    return cartItems
        .map((e) =>
            e.productName +
            e.attributes.map((e) => '${e.name}: ${e.value}').join(', '))
        .join('; ');
  }
}
