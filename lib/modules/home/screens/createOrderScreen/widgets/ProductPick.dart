import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:omnichannel_flutter/data/modals/Order.dart';
import 'package:omnichannel_flutter/modules/home/screens/createExportScreen/widget/PickProductDialog.dart';
import 'package:omnichannel_flutter/modules/order/bloc/CreateOrderBloc.dart';
import 'package:omnichannel_flutter/modules/order/bloc/CreateOrderEvent.dart';
import 'package:omnichannel_flutter/modules/order/bloc/CreateOrderState.dart';

extension FicListExtension<T> on List<T> {

  /// Maps each element of the list.
  /// The [map] function gets both the original [item] and its [index].
  Iterable<E> mapIndexed<E>(E Function(int index, T item) map) sync* {
    for (var index = 0; index < length; index++) {
      yield map(index, this[index]);
    }
  }
}

class ProductPick extends StatefulWidget {
  const ProductPick(this.createOrderBloc);

  final CreateOrderBloc createOrderBloc;

  @override
  State<StatefulWidget> createState() => _State();
}

class _State extends State<ProductPick> {
  _pickProductDialog() {
    showDialog(
      context: context,
      builder: (context) {
        return PickProductDialog(
          onPressItem: (product) {
            log(product.attributes.toString());
            widget.createOrderBloc.add(UpdatePayloadAddProduct(CartItem(
              productIdRef: product.productIdRef,
              variantId: product.variantId,
              price: product.price,
              weight: product.weight,
              productName: product.name,
              attributes: product.attributes,
              qty: 1,
            )));
          },
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Sản phẩm',
          style: TextStyle(
            fontSize: 14,
            color: Colors.grey,
          ),
        ),
        Container(
          padding: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            children: [
              Expanded(
                flex: 1,
                child: InkWell(
                  onTap: _pickProductDialog,
                  child: Text('Chọn sản phẩm',
                      style: TextStyle(
                          fontSize: 16,
                          color: Colors.black,
                          fontWeight: FontWeight.w500)),
                ),
              ),
              Icon(Icons.arrow_drop_down),
            ],
          ),
        ),
        Divider(height: 0, thickness: 1.2, color: Colors.grey),
        SizedBox(
          height: 10,
        ),
        BlocBuilder<CreateOrderBloc, CreateOrderState>(
          builder: (context, state) => Table(
            columnWidths: {
              0: FlexColumnWidth(6),
              1: FlexColumnWidth(3),
              2: FlexColumnWidth(2),
            },
            border: TableBorder.all(color: Colors.black12),
            children: [
              TableRow(children: [
                TableCell(
                  verticalAlignment: TableCellVerticalAlignment.middle,
                  child: Text('Sản phẩm'),
                ),
                TableCell(
                  verticalAlignment: TableCellVerticalAlignment.middle,
                  child: Text('Giá'),
                ),
                TableCell(
                  verticalAlignment: TableCellVerticalAlignment.middle,
                  child: Text('SL'),
                )
              ]),
              ...List.from(
                  state.payload.cartItems?.mapIndexed((index, e) => TableRow(children: [
                        TableCell(
                            verticalAlignment:
                                TableCellVerticalAlignment.middle,
                            child: Text(e.productName +
                                '\n' +
                                e.attributes
                                    .map((attr) => attr.toStringDisplay())
                                    .join('; '))),
                        TableCell(
                            verticalAlignment:
                                TableCellVerticalAlignment.middle,
                            child: Text(e.price?.toString() ?? 'Trống')),
                        TableCell(
                          verticalAlignment: TableCellVerticalAlignment.middle,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              InkWell(
                                onTap: () => widget.createOrderBloc.add(UpdatePayloadRemoveQuantity(index)),
                                child: Icon(
                                  Icons.remove,
                                  size: 20,
                                ),
                              ),
                              Text(e.qty?.toString() ?? ''),
                              InkWell(
                                onTap: () => widget.createOrderBloc.add(UpdatePayloadAddQuantity(index)),
                                child: Icon(
                                  Icons.add,
                                  size: 20,
                                ),
                              )
                            ],
                          ),
                        ),
                      ])))
            ],
          ),
        )
      ],
    );
  }
}
