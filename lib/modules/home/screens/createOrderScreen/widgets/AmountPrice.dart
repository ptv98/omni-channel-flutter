import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:omnichannel_flutter/constant/Metrics.dart';
import 'package:omnichannel_flutter/modules/order/bloc/CreateOrderBloc.dart';
import 'package:omnichannel_flutter/modules/order/bloc/CreateOrderEvent.dart';
import 'package:omnichannel_flutter/modules/order/bloc/CreateOrderState.dart';
import 'package:omnichannel_flutter/utis/ui/main.dart';
import 'package:omnichannel_flutter/widgets/Button/main.dart';

extension CalculatePrice on AmountPrice {
  calcAmount() {}
}

class AmountPrice extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CreateOrderBloc, CreateOrderState>(
      builder: (context, state) {
        final payload = state.payload;
        final priceOfProducts = payload.cartItems
            .fold(0, (sum, element) => sum + element.price * element.qty);
        final discount = payload.discount ?? 0;
        final pay =
            payload.bankPayment + payload.cardPayment + payload.otherPayment;

        return Positioned(
          child: Container(
            padding: EdgeInsets.all(8),
            width: Metrics.getScreenWidth(context),
            decoration: BoxDecoration(color: Colors.white, boxShadow: [
              BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 0,
                  blurRadius: 1,
                  offset: Offset(0, -2))
            ]),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 0),
                  child: Row(
                    children: [
                      Text(
                        'Tiền hàng: ' + priceOfProducts.toString(),
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 0),
                  child: Row(
                    children: [
                      Text('Giảm giá: ' + discount.toString()),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 0),
                  child: Row(
                    children: [
                      Text('Đã thanh toán: ' + pay.toString()),
                    ],
                  ),
                ),
                SizedBox(
                  height: 4,
                ),
                Text(
                  'Tổng: ${priceOfProducts - discount - pay}',
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 2, left: 16, right: 16),
                  child: SizedBox(
                    width: double.infinity,
                    child: AppButton(
                      onPressed: () => BlocProvider.of<CreateOrderBloc>(context)
                          .add(CreateOrderRequest(callback: (isSuccess) {
                        if (isSuccess) {
                          showSnackBar(context,
                              text: 'Tạo đơn hàng thành công',
                              color: Colors.blue);
                          Navigator.pop(context, true);
                        } else {
                          showSnackBar(context,
                              text:
                                  'Tạo đơn hàng thất bại. Vui lòng thử lại hoặc liên hệ admin');
                        }
                      })),
                      title: "Xác nhận",
                    ),
                  ),
                )
              ],
            ),
          ),
          bottom: 0,
        );
      },
    );
  }
}
