import 'package:omnichannel_flutter/modules/home/screens/createOrderScreen/createOrderScreen.dart';

class MineTypeWithName {
  const MineTypeWithName({this.name, this.value});
  final String name;
  final int value;
}

extension ProcessMineType on createOrderScreenState {
  List<MineTypeWithName> getMineType() {
    return [
      MineTypeWithName(name: 'Bán tại shop', value: 1),
      MineTypeWithName(
        name: 'Giao hàng thu hộ',
        value: 2,
      ),
      MineTypeWithName(
        name: 'Giao hàng ứng tiền',
        value: 3,
      )
    ];
  }
}
