import 'dart:developer';
import 'dart:io';

import 'package:after_layout/after_layout.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:omnichannel_flutter/bloc/LocationData/LocationDataBloc.dart';
import 'package:omnichannel_flutter/bloc/LocationData/LocationDataState.dart';
import 'package:omnichannel_flutter/constant/Metrics.dart';
import 'package:omnichannel_flutter/constant/Status.dart';
import 'package:omnichannel_flutter/modules/home/bloc/CreateImportExport/CreateImportExportBloc.dart';
import 'package:omnichannel_flutter/modules/home/bloc/CreateImportExport/CreateImportExportEvent.dart';
import 'package:omnichannel_flutter/modules/home/screens/createExportScreen/widget/normal_text_input.dart';
import 'package:omnichannel_flutter/modules/home/screens/createOrderScreen/widgets/AmountPrice.dart';
import 'package:omnichannel_flutter/modules/home/screens/createOrderScreen/widgets/ProductPick.dart';
import 'package:omnichannel_flutter/modules/home/screens/storehouse/bloc/StorehouseBloc.dart';
import 'package:omnichannel_flutter/modules/home/screens/storehouse/bloc/StorehouseEvent.dart';
import 'package:omnichannel_flutter/modules/home/screens/storehouse/bloc/StorehouseState.dart';
import 'package:omnichannel_flutter/modules/order/bloc/CreateOrderBloc.dart';
import 'package:omnichannel_flutter/modules/order/bloc/CreateOrderEvent.dart';
import 'package:omnichannel_flutter/modules/order/bloc/CreateOrderState.dart';
import 'package:omnichannel_flutter/modules/product/screens/CreateProduct/widgets/ProductCreateFormWrapper.dart';
import 'package:omnichannel_flutter/modules/stock/widgets/LocationPicker/LocationPicker.dart';
import 'package:omnichannel_flutter/normal_dialog_list/normal_dialog_list.dart';
import 'package:omnichannel_flutter/utis/ui/Shadow.dart';
import 'package:omnichannel_flutter/widgets/Button/main.dart';
import 'package:omnichannel_flutter/widgets/CustomAppBar/main.dart';
import 'createOrderScreen.extensions.dart';

const _sellStyles = ['Người mua nhận hàng', 'Giao hàng cho người khác'];

class createOrderScreen extends StatefulWidget {
  @override
  createOrderScreenState createState() => createOrderScreenState();
}

class createOrderScreenState extends State<createOrderScreen>
    with AfterLayoutMixin {
  List<String> _listWareHouse = ["Kho 1", "Kho 2", "Kho 3"];
  List<String> _listDistrict = ["Ha Noi", "TPHCM", "Da Nang"];
  List<String> _listHuyen = ["Hoang Mai", "Hai Ba Trung", "Cau Giay"];
  List<String> _listXa = ["Hien Luong", "Ha Hoa", "Vinh Tuy"];
  List<String> _listItem = [
    "Mũ",
    "Áo sơ mi",
    "Giày",
    "Quần",
    "abcsad",
    "dasasd",
    'ahsdasd',
    "asdasda",
    "asdadsasd",
    "qweqweqwe",
    "qwewrqeqwe",
    "asdasdasd"
  ];
  TextEditingController discountController = new TextEditingController();
  TextEditingController placeController = new TextEditingController();
  TextEditingController firstPayController = new TextEditingController();
  TextEditingController secondPayController = new TextEditingController();
  TextEditingController thirdPayController = new TextEditingController();
  TextEditingController styleController = new TextEditingController();
  TextEditingController _phoneNumberController = new TextEditingController();
  TextEditingController _nameController = new TextEditingController();
  TextEditingController _receverNumber = new TextEditingController();
  TextEditingController _receverName = new TextEditingController();
  TextEditingController _storeNoteController = new TextEditingController();
  TextEditingController _customerNoteContro = new TextEditingController();
  TextEditingController quantityController = new TextEditingController();
  TextEditingController _ = new TextEditingController();
  String errPhone = null;
  String errName = null;
  String errText = null;
  String quantityErr = null;
  String styleErr = null;
  String errNameRecever = null;
  String errPhoneRecever = null;
  String _wareHouse = "";
  String _styleSell = "";
  String _item = "";
  String _district = "";
  String _styleShip = "";
  String _huyen = "";
  String _xa = "";
  String _place = "";
  File _image;

  ///sẽ có một trường giá khi cọn sản phẩm cái này ghép API các bạn xử lý nhé mình fix tạm giá ntn

  int price = 3000000;
  final picker = ImagePicker();

  _onPressListItem() {
    FocusScope.of(context).unfocus();
    List<String> listData = [];
    if (_listItem != null && _listItem.length > 0) {
      _listItem.forEach((bf) {
        listData.add(bf);
      });
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return NormalDialogListBank(
              title: 'Chọn sản phẩm',
              listData: listData,
              onSelectedItem: _onChangeItem,
              selectedData: _item != null ? _item : null,
            );
          });
    }
  }

  void initState() {
    super.initState();
    _wareHouse = _listWareHouse[0];
    _item = _listItem[0];
    _styleSell = _sellStyles[0];
    _styleShip = _sellStyles[0];
    _district = _listDistrict[0];
    _huyen = _listHuyen[0];
    _xa = _listXa[0];
    quantityController.text = 1.toString();
  }

  _onChangeWareHouse(String value) {
    setState(() {
      _wareHouse = value;
    });
  }

  _onChangeStyleShip(String value) {
    setState(() {
      _styleShip = value;
    });
  }

  _onChangeItem(String item) {
    setState(() {
      _item = item;
    });
  }

  @override
  void afterFirstLayout(BuildContext context) {
    final stockBloc = BlocProvider.of<StorehouseBloc>(context);
    final createBloc = BlocProvider.of<CreateOrderBloc>(context);

    if (stockBloc.state.stocks.isNotEmpty) {
      createBloc.add(UpdatePayloadStockId(stockBloc.state.stocks.first.id));
    } else {
      stockBloc.stream.listen((event) {
        if (event.status == Status.success) {
          createBloc.add(UpdatePayloadStockId(stockBloc.state.stocks.first.id));
        }
      });
      stockBloc.add(StoreHouseEventGetStocks());
    }
  }

  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }

  Widget infoCustomer(CreateOrderBloc createOrderBloc) {
    return BlocBuilder<LocationDataBloc, LocationDataState>(
        builder: (context, locationState) =>
            BlocBuilder<CreateOrderBloc, CreateOrderState>(
              bloc: createOrderBloc,
              builder: (context, createOrderState) => Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Chọn hình thức nhận hàng",
                      style: TextStyle(color: Colors.grey),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    DropdownButton(
                      isExpanded: true,
                      underline: Divider(
                          height: 0, thickness: 1.5, color: Colors.grey),
                      value: _styleShip,
                      style: TextStyle(
                        fontSize: 16,
                        color: Colors.black,
                      ),
                      items: _sellStyles.map((item) {
                        return DropdownMenuItem(
                          child: Text(
                            item,
                            style: TextStyle(
                              fontSize: 16,
                              color: Colors.black,
                            ),
                          ),
                          value: item,
                        );
                      }).toList(),
                      onChanged: _onChangeStyleShip,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    (_styleShip != _sellStyles[0])
                        ? Column(
                            children: [
                              NormalTextInput(
                                onChanged: (text) {
                                  createOrderBloc.add(
                                      UpdatePayloadRecipientPhoneNumber(text));
                                  setState(() {
                                    errPhoneRecever = null;
                                  });
                                },
                                color: Colors.black,
                                errorText: errPhoneRecever,
                                fontSize: 16,
                                controller: _receverNumber,
                                keyboardType: TextInputType.number,
                                hintText: ("Số điện thoại người nhận *"),
                                hintTextFontSize: 14,
                                contentPadding:
                                    EdgeInsets.symmetric(vertical: 10),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              NormalTextInput(
                                onChanged: (text) {
                                  createOrderBloc
                                      .add(UpdatePayloadRecipientName(text));
                                  setState(() {
                                    errNameRecever = null;
                                  });
                                },
                                color: Colors.black,
                                errorText: errNameRecever,
                                fontSize: 16,
                                controller: _receverName,
                                keyboardType: TextInputType.name,
                                hintText: ("Tên người nhận *"),
                                hintTextFontSize: 14,
                                contentPadding:
                                    EdgeInsets.symmetric(vertical: 10),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                            ],
                          )
                        : SizedBox.shrink(),
                    LocationPicker(
                      data: locationState.city.data,
                      hint: 'Chọn thành phố',
                      code: createOrderState.payload.cityCode,
                      status: locationState.city.status,
                      // hasError: createStockState.error.cityError != null,
                      onChanged: (value) {
                        createOrderBloc.add(UpdatePayloadCity(value));
                      },
                    ),
                    LocationPicker(
                      data: locationState.district.data,
                      hint: 'Chọn quận/huyện',
                      // code: createStockState.createOneStockInput.cityCode,
                      code: createOrderState.payload.districtCode,
                      status: locationState.district.status,
                      // hasError: createStockState.error.cityError != null,
                      onChanged: (value) {
                        createOrderBloc.add(UpdatePayloadDistrict(value));
                      },
                    ),
                    LocationPicker(
                      data: locationState.ward.data,
                      hint: 'Chọn phường/xã',
                      // code: createStockState.createOneStockInput.cityCode,
                      code: createOrderState.payload.wardCode,
                      status: locationState.ward.status,
                      // hasError: createStockState.error.cityError != null,
                      onChanged: (value) {
                        createOrderBloc.add(UpdatePayloadWard(value));
                      },
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    NormalTextInput(
                      onChanged: (text) =>
                          createOrderBloc.add(UpdatePayloadAddress(text)),
                      color: Colors.black,
                      //  errorText: errPhoneRecever,
                      fontSize: 16,
                      controller: placeController,
                      keyboardType: TextInputType.name,
                      hintText: ("Địa chỉ"),
                      hintTextFontSize: 14,
                      contentPadding: EdgeInsets.symmetric(vertical: 10),
                    ),
                  ],
                ),
              ),
            ));
  }

  _showBottomSheet() {
    showModalBottomSheet(
        context: context,
        backgroundColor: Colors.transparent,
        builder: (BuildContext context) {
          return Container(
            height: MediaQuery.of(context).size.height * 0.3,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(16), topLeft: Radius.circular(16)),
            ),
            child: Container(
              padding:
                  EdgeInsets.only(top: 50, bottom: 20, right: 30, left: 30),
              child: Column(
                children: [
                  InkWell(
                    onTap: () async {
                      final pickedFile =
                          await picker.getImage(source: ImageSource.camera);

                      setState(() {
                        if (pickedFile != null) {
                          _image = File(pickedFile.path);
                          Navigator.pop(context);
                        } else {
                          print('No image selected.');
                        }
                      });
                    },
                    child: Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          color: Colors.grey.shade50),
                      padding: EdgeInsets.all(8),
                      child: Row(
                        children: [
                          Icon(
                            Icons.camera_alt,
                            size: 30,
                            color: Colors.black,
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Text(
                            "Chụp ảnh từ Camera",
                            style: TextStyle(color: Colors.black, fontSize: 18),
                          )
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  InkWell(
                    onTap: () async {
                      final pickedFile =
                          await picker.getImage(source: ImageSource.gallery);

                      setState(() {
                        if (pickedFile != null) {
                          _image = File(pickedFile.path);
                          Navigator.pop(context);
                        } else {
                          print('No image selected.');
                        }
                      });
                    },
                    child: Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          color: Colors.grey.shade50),
                      padding: EdgeInsets.all(8),
                      child: Row(
                        children: [
                          Icon(
                            Icons.photo,
                            size: 30,
                            color: Colors.black,
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Text(
                            "Chọn ảnh từ thư viện",
                            style: TextStyle(color: Colors.black, fontSize: 18),
                          )
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          );
        });
  }

  _onPressSubmit() {
    FocusScope.of(context).requestFocus(FocusNode());
    var discount = int.parse(discountController.text);
    assert(discount is int);
    var firstPay = int.parse(firstPayController.text);
    assert(firstPay is int);
    var secondPay = int.parse(secondPayController.text);
    assert(secondPay is int);
    var thirdPay = int.parse(thirdPayController.text);
    assert(thirdPay is int);
    var lastPrice = price - discount - firstPay - secondPay - thirdPay;

    ///chỗ này các bạn tự validate nhé
    Dialog dialog = Dialog(
      child: Container(
        padding: EdgeInsets.all(10),
        width: MediaQuery.of(context).size.width - 200,
        height: 300,
        child: Column(
          children: [
            Text(
              "Có phải bạn muốn tạo đơn hàng sản phẩm ${_item.toString()} với giá ${lastPrice.toString()} đ",
              style: TextStyle(fontSize: 16),
            ),
            SizedBox(
              height: 20,
            ),
            Spacer(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                InkWell(
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.pop(context, true);
                  },
                  child: Container(
                    width: 120,
                    height: 40,
                    decoration: BoxDecoration(
                        color: Colors.blueAccent,
                        borderRadius: BorderRadius.circular(10)),
                    child: Center(
                        child: Text(
                      "OK",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.w500),
                    )),
                  ),
                ),
                InkWell(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Container(
                    width: 120,
                    height: 40,
                    decoration: BoxDecoration(
                        color: Colors.blueGrey,
                        borderRadius: BorderRadius.circular(10)),
                    child: Center(
                        child: Text(
                      "Cancel",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.w500),
                    )),
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return dialog;
        });
  }

  @override
  Widget build(BuildContext context) {
    final createOrderBloc = BlocProvider.of<CreateOrderBloc>(context);

    return Scaffold(
      appBar: CustomAppBar(
        title: 'Tạo đơn hàng',
      ),
      body: Container(
        color: Colors.white,
        child: SafeArea(
          bottom: true,
          child: Stack(
            children: [
              GestureDetector(
                onPanDown: (_) => FocusScope.of(context).requestFocus(FocusNode()),
                child: SingleChildScrollView(
                  child: BlocBuilder<CreateOrderBloc, CreateOrderState>(
                    builder: (context, state) => Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        ProductCreateFormWrapper(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Chọn hình thức mua hàng",
                                style: TextStyle(color: Colors.grey),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              DropdownButton(
                                isExpanded: true,
                                underline: Divider(
                                    height: 0,
                                    thickness: 1.5,
                                    color: Colors.grey),
                                value: state.payload.minetype,
                                style: TextStyle(
                                  fontSize: 16,
                                  color: Colors.black,
                                ),
                                items: getMineType().map((item) {
                                  return DropdownMenuItem(
                                    child: Text(
                                      item.name,
                                      style: TextStyle(
                                        fontSize: 16,
                                        color: Colors.black,
                                      ),
                                    ),
                                    value: item.value,
                                  );
                                }).toList(),
                                onChanged: (e) {
                                  createOrderBloc.add(UpdatePayloadMineType(e));
                                },
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              NormalTextInput(
                                onChanged: (text) {
                                  createOrderBloc
                                      .add(UpdatePayloadPhoneNumber(text));
                                  setState(() {
                                    errPhone = null;
                                  });
                                },
                                color: Colors.black,
                                errorText: errPhone,
                                fontSize: 16,
                                controller: _phoneNumberController,
                                keyboardType: TextInputType.number,
                                hintText: ("Số điện thoại khách hàng *"),
                                hintTextFontSize: 14,
                                contentPadding:
                                EdgeInsets.symmetric(vertical: 10),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              NormalTextInput(
                                onChanged: (text) {
                                  createOrderBloc
                                      .add(UpdatePayloadCustomerName(text));
                                  setState(() {
                                    errName = null;
                                  });
                                },
                                color: Colors.black,
                                errorText: errName,
                                fontSize: 16,
                                controller: _nameController,
                                keyboardType: TextInputType.name,
                                hintText: ("Tên khách hàng *"),
                                hintTextFontSize: 14,
                                contentPadding:
                                EdgeInsets.symmetric(vertical: 10),
                              ),
                            ],
                          ),
                        ),
                        state.payload.minetype != 1
                            ? ProductCreateFormWrapper(
                          child: infoCustomer(createOrderBloc),
                        )
                            : SizedBox.shrink(),
                        ProductCreateFormWrapper(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              ProductPick(createOrderBloc),
                              SizedBox(
                                height: 20,
                              ),
                              Text(
                                "Chọn kho xuất hàng",
                                style: TextStyle(color: Colors.grey),
                              ),
                              BlocBuilder<StorehouseBloc, StorehouseState>(
                                  builder: (context, stockState) =>
                                      DropdownButton(
                                        isExpanded: true,
                                        underline: Divider(
                                            height: 0,
                                            thickness: 1.5,
                                            color: Colors.grey),
                                        value: state.payload.stockId,
                                        style: TextStyle(
                                          fontSize: 16,
                                          color: Colors.black,
                                        ),
                                        items: stockState.stocks.map((item) {
                                          return DropdownMenuItem(
                                            child: Text(
                                              item.name,
                                              style: TextStyle(
                                                fontSize: 16,
                                                color: Colors.black,
                                              ),
                                            ),
                                            value: item.id,
                                          );
                                        }).toList(),
                                        onChanged: (id) {
                                          _onChangeWareHouse(id);
                                        },
                                      )),
                            ],
                          ),
                        ),
                        ProductCreateFormWrapper(
                          child: Column(
                            children: [
                              NormalTextInput(
                                onChanged: (String text) {
                                  createOrderBloc
                                      .add(UpdatePayloadDiscount(text.isEmpty ? 0 : int.parse(text)));
                                  setState(() {
                                    errText = null;
                                  });
                                },
                                color: Colors.black,
                                errorText: errText,
                                fontSize: 16,
                                controller: discountController,
                                keyboardType: TextInputType.number,
                                hintText: ("Giảm giá"),
                                hintTextFontSize: 14,
                                contentPadding:
                                EdgeInsets.symmetric(vertical: 10),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              NormalTextInput(
                                onChanged: (text) {
                                  createOrderBloc
                                      .add(UpdatePayloadBankPayment(int.parse(text)));
                                  setState(() {
                                    quantityErr = null;
                                  });
                                },
                                color: Colors.black,
                                //   errorText: quantityErr,
                                fontSize: 16,
                                controller: firstPayController,
                                keyboardType: TextInputType.number,
                                hintText: ("Chuyển khoản"),
                                hintTextFontSize: 14,
                                contentPadding:
                                EdgeInsets.symmetric(vertical: 10),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              NormalTextInput(
                                onChanged: (text) {
                                  createOrderBloc
                                      .add(UpdatePayloadCardPayment(int.parse(text)));
                                  setState(() {
                                    quantityErr = null;
                                  });
                                },
                                color: Colors.black,
                                //  errorText: quantityErr,
                                fontSize: 16,
                                controller: secondPayController,
                                keyboardType: TextInputType.number,
                                hintText: ("Đã quẹt thẻ"),
                                hintTextFontSize: 14,
                                contentPadding:
                                EdgeInsets.symmetric(vertical: 10),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              NormalTextInput(
                                onChanged: (text) {
                                  createOrderBloc
                                      .add(UpdatePayloadOtherPayment(int.parse(text)));
                                  setState(() {
                                    quantityErr = null;
                                  });
                                },
                                color: Colors.black,
                                //  errorText: quantityErr,
                                fontSize: 16,
                                controller: thirdPayController,
                                keyboardType: TextInputType.number,
                                hintText: ("Hình thức khác"),
                                hintTextFontSize: 14,
                                contentPadding:
                                EdgeInsets.symmetric(vertical: 10),
                              ),
                            ],
                          ),
                        ),
                        ProductCreateFormWrapper(
                          child: NormalTextInput(
                            onChanged: (text) {
                              createOrderBloc.add(UpdatePayloadInternalNote(text));
                            },
                            color: Colors.black,
                            //  errorText: errPhoneRecever,
                            fontSize: 16,
                            alignLabelWithHint: true,
                            controller: _storeNoteController,
                            keyboardType: TextInputType.name,
                            hintText: ("Ghi chú nội bộ"),
                            hintTextFontSize: 14,
                            contentPadding: EdgeInsets.symmetric(vertical: 10),
                            maxLines: 4,
                          ),
                        ),
                        ProductCreateFormWrapper(
                          child: NormalTextInput(
                            onChanged: (text) {
                              createOrderBloc.add(UpdatePayloadCustomerNote(text));
                            },
                            color: Colors.black,
                            //  errorText: errPhoneRecever,
                            fontSize: 16,
                            alignLabelWithHint: true,
                            controller: _customerNoteContro,
                            keyboardType: TextInputType.name,
                            hintText: ("Ghi chú khách "),
                            hintTextFontSize: 14,
                            contentPadding: EdgeInsets.symmetric(vertical: 10),
                            maxLines: 4,
                          ),
                        ),
                        // SizedBox(height: 10),
                        // Row(
                        //   children: [
                        //     Text(
                        //       "Chụp ảnh sản phẩm",
                        //       style: TextStyle(
                        //         fontSize: 14,
                        //       ),
                        //     ),
                        //     Spacer(),
                        //     InkWell(
                        //       onTap: () async {
                        //         _showBottomSheet();
                        //       },
                        //       child: Icon(
                        //         Icons.add_a_photo_outlined,
                        //         size: 40,
                        //         color: Colors.blueAccent,
                        //       ),
                        //     ),
                        //   ],
                        // ),
                        // SizedBox(
                        //   height: 5,
                        // ),
                        // Container(
                        //   decoration: BoxDecoration(
                        //       color: Colors.grey.shade300,
                        //       borderRadius: BorderRadius.all(Radius.circular(16))),
                        //   width: double.infinity,
                        //   height: 300,
                        //   child: _image != null
                        //       ? ClipRRect(
                        //           borderRadius: BorderRadius.all(Radius.circular(16)),
                        //           child: Image.file(
                        //             _image,
                        //             fit: BoxFit.cover,
                        //           ))
                        //       : Icon(
                        //           Icons.photo,
                        //           size: 50,
                        //         ),
                        // ),
                        // SizedBox(
                        //   height: 20,
                        // ),
                        SizedBox(
                          height: 150,
                        )
                      ],
                    ),
                  ),
                ),
              ),
              AmountPrice()
            ],
          ),
        ),
      ),
    );
  }
}
