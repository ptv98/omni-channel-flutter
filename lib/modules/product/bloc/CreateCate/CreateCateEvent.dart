import 'package:flutter/cupertino.dart';

class CreateCateEvent {
  const CreateCateEvent({@required this.name});
  final String name;
}