class PngJpg {
  static const onBoardCreate = 'lib/assets/png-jpg/on-board-create.png';
  static const onBoardFree = 'lib/assets/png-jpg/on-board-free.jpeg';
  static const onBoardOmni = 'lib/assets/png-jpg/on-board-omni.jpg';
  static const imgNullData = 'lib/assets/png-jpg/img_null_data.png';
  static const ic_face = 'lib/assets/png-jpg/ic_facebook.png';
  static const ic_tiktok = 'lib/assets/png-jpg/ic_tiktok.png';
  static const ic_insta = 'lib/assets/png-jpg/ic_insta.png';
  static const ic_clear_text = 'lib/assets/png-jpg/ic_clear_text.png';
  static const ic_search = 'lib/assets/png-jpg/ic_search.png';
  static const ic_close = 'lib/assets/png-jpg/ic_close.png';
}