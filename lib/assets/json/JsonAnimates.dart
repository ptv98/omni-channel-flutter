
class JsonAnimates {
  static const settings = 'lib/assets/json/settings.json';
  static const update = 'lib/assets/json/update.json';
  static const money = 'lib/assets/json/money.json';
  static const add = 'lib/assets/json/add.json';
  static const loading = 'lib/assets/json/loading.json';
  static const loadingV2 = 'lib/assets/json/loadingV2.json';
  static const loadingCircles = 'lib/assets/json/9419-loading-circles.json';
}