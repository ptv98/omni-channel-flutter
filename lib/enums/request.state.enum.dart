enum RequestState {
  loading,
  fail,
  success,
  unknown,
}