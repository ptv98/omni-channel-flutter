
import 'package:flutter/cupertino.dart';
import 'package:omnichannel_flutter/modals/home-modals.dart';

abstract class BaseScreen extends StatelessWidget {

}

abstract class BaseScreenStateful extends StatefulWidget {

}