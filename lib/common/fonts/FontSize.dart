class FontSize {
  static final small = 10.0;
  static final medium = 12.0;
  static final big = 18.0;
  static final soBig = 20.0;
}